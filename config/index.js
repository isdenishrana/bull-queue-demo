const path = require("path");
require("dotenv-safe").config({
  path: path.join(__dirname, "../.env"),
  sample: path.join(__dirname, "../.env.example"),
  allowEmptyValues: true,
});

module.exports = {
  mongoUrl: process.env.MONGO_URL,
  mongoDbOptions: {
    keepAlive: true,
    useNewUrlParser: true,
    useUnifiedTopology: true,
  },
};
