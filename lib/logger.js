class Logger {
  error(err) {
    console.error(err);
  }

  info(message) {
    console.info(message);
  }
}

module.exports = new Logger();
