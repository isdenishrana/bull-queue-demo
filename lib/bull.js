const Bull = require('bull');
const BullHelper = require('../helpers/bull');

const testQueue = new Bull('test', 'localhost');

/**
 * Queue always work on producer and worker.
 * As we know that worker is always listing on redis TCP connection for job. So we have added worker function in the construction of the class.
 * That's way worker start listing automatically without any function call.
 * 
 * Please read below code for better understanding.
 */
class Queue extends BullHelper {
  constructor() {
    super();
    testQueue.process(async (job) => {
      return this.processTestQueue(job); // This is the job worker.
    })
    testQueue.on('error', (error) => {
      console.log(error);
    })
    testQueue.on('completed', (job) => {
      console.log(job);
    })
  }

  async sendToQueue(data) {
    await testQueue.add(data, { delay: 0, attempts: 5, backoff: { type: 'fixed', delay: 60000 } }); // This is the job producer.
  }
}

module.exports = new Queue();
