const mongoose = require("mongoose");
const logger = require("./logger");
const { mongoUrl, mongoDbOptions } = require("../config");

mongoose.Promise = global.Promise;

const openMongoConnection = () => {
  mongoose.connect(mongoUrl, mongoDbOptions);
};

mongoose.connection.on("open", () => {
  logger.info("Database connected successfully");
});

mongoose.connection.on("error", (err) => {
  logger.error("mongodb connection error : ", err);
});

const closeConnection = () => {
  mongoose.connection.close(() => {
    logger.info("Mongoose disconnected on app termination");
    process.exit(0);
  });
};

process.on("SIGINT", closeConnection);

module.exports = {
  mongoose,
  openMongoConnection,
};
