const express = require("express");
const userRoutes = require("./routes/user");
const { openMongoConnection } = require("./lib/mongoose");

const app = express();

openMongoConnection();

app.use(express.json());

app.use("/users", userRoutes);

const PORT = 4000;

app.listen(PORT, () => console.log(`App is running on ${PORT}...`));
