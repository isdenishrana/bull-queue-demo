const bullLib = require('../lib/bull');

const User = require("../models/User");

exports.create = async (req, res) => {
  const { name, email } = req.body;

  try {
    const user = await User.create({
      name,
      email,
    });

    // await sendEmailCreationEmail({ name, email });
    await bullLib.sendToQueue({ email })

    res.json(user);
  } catch (error) {
    console.log(error);
    res.status(400).json(error);
  }
};
